FROM openjdk:17
ADD ./target/mainapp-0.0.1-SNAPSHOT.jar /usr/src/mainapp-0.0.1-SNAPSHOT.jar
WORKDIR usr/src
ENTRYPOINT ["java","-jar", "mainapp-0.0.1-SNAPSHOT.jar"]
