package com.pusproject.services;

import com.pusproject.dtos.ResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

public abstract class OverallService {

    protected final ResponseDTO responseDTO = new ResponseDTO();

    protected void setResponse(String message, HttpStatus status) {

        responseDTO.setResponse(message, status);
    }

    public String getMessage() {

        return responseDTO.getMessage();
    }

    public HttpStatus getStatus() {

        return responseDTO.getStatus();
    }

}
