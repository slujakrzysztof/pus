package com.pusproject.services;

import com.pusproject.dtos.CommentDTO;
import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.mappers.CommentMapper;
import com.pusproject.models.Comment;
import com.pusproject.models.Project;
import com.pusproject.models.Task;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.CommentRepository;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.TaskRepository;
import com.pusproject.repositories.UserProfileRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Setter
@Getter
public class CommentService extends OverallService{

    private final CommentRepository commentRepository;
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final UserProfileRepository userProfileRepository;
    private final CommentMapper commentMapper;


    private boolean isUserMemberOfTheProject(int projectId, UserProfileDTO userProfileDTO) {

        return userProfileDTO.getProjectsId().contains(projectId);
    }

    private void addComment(CommentDTO commentDTO, Task task, UserProfileDTO actualUser) {

        Comment comment = commentMapper.mapToComment(commentDTO, task, actualUser);
        commentRepository.save(comment);
        setResponse("Comment created", HttpStatus.CREATED);
    }




    public void addComment(CommentDTO commentDTO, UserProfileDTO actualUser) {

        Optional<Task> task = taskRepository.findById(commentDTO.getTaskId());

        task.ifPresentOrElse(t -> {

                Optional<Project> project = projectRepository.findByTaskId(t.getId());

                if (project.isPresent()
                        && project.get().getId() == commentDTO.getProjectId()
                        && isUserMemberOfTheProject(project.get().getId(), actualUser)) {

                    addComment(commentDTO, t, actualUser);
                }

        }, () -> setResponse(String.format("Task with id %d not found", commentDTO.getTaskId()), HttpStatus.NOT_FOUND));

    }

    public List<CommentDTO> getTaskComments(int taskId) {

        Optional<List<Comment>> comments = commentRepository.findByTaskId(taskId);

        return comments.map(commentList -> commentList
                .stream()
                .map(c -> commentMapper.mapToCommentDTO(c))
                .collect(Collectors.toList())).orElseGet(List::of);
    }

    public void deleteComment(int commentId, int projectId, UserProfileDTO loggedUser) {

        Optional<Comment> comment = commentRepository.findById(commentId);

        comment.ifPresentOrElse(c -> {

            if(isUserMemberOfTheProject(projectId, loggedUser))
                commentRepository.deleteById(commentId);
            setResponse(String.format("Comment with id = %s deleted",commentId), HttpStatus.ACCEPTED);
        }, () -> setResponse(String.format("Comment with id = %s not found",commentId), HttpStatus.NOT_FOUND));

    }
}
