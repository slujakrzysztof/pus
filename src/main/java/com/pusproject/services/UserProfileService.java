package com.pusproject.services;

import com.pusproject.dtos.ActivationCodeDTO;
import com.pusproject.dtos.LoggingUserDTO;
import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.exceptions.ConfirmationException;
import com.pusproject.exceptions.RegistrationException;
import com.pusproject.mappers.AddressMapper;
import com.pusproject.mappers.UserProfileMapper;
import com.pusproject.models.ActivationCode;
import com.pusproject.models.Address;
import com.pusproject.models.Project;
import com.pusproject.models.UserProfile;
import com.pusproject.properties.PropertyReader;
import com.pusproject.repositories.ActivationCodeRepository;
import com.pusproject.repositories.AddressRepository;
import com.pusproject.repositories.UserProfileRepository;
import com.pusproject.timer.ActivationCodeTimer;
import com.pusproject.validators.UserProfileValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Setter
@Getter
public class UserProfileService extends OverallService{

    private final UserProfileRepository userProfileRepository;
    private final AddressRepository addressRepository;
    private final UserProfileValidator userProfileValidator;
    private final ActivationCodeRepository activationCodeRepository;
    private final PropertyReader propertyReader;
    private List<ActivationCodeDTO> activationCodeDTOList = new ArrayList<>();
    private final UserProfileMapper userProfileMapper;
    private final AuthService authService;

    private UserProfile modifyUser(UserProfile userProfile,
                                   UserProfileDTO actualUserDTO) {

        UserProfile modifiedUser = userProfileMapper.mapToUserProfile(actualUserDTO, true);

        userProfile.setName(modifiedUser.getName());
        userProfile.setSurname(modifiedUser.getName());
        userProfile.setPassword(modifiedUser.getPassword());

        return userProfile;
    }

    private UserProfile modifyUser(UserProfileDTO modifiedUserDTO,
                                      UserProfileDTO actualUserDTO) {

        if (modifiedUserDTO.getEmail().equals(actualUserDTO.getEmail())) {

            Optional<UserProfile> userProfile = userProfileRepository.findById(actualUserDTO.getId());

            if (userProfile.isPresent()) {

                UserProfile user = userProfile.get();
                user = modifyUser(user, actualUserDTO);
                return user;
            }
        }

        return null;
    }

    public UserProfile getUserProfile(LoggingUserDTO loggingUserDTO) {

        Optional<UserProfile> userProfile = userProfileRepository.findByEmail(loggingUserDTO.getEmail());

        if(userProfile.isPresent())
            return userProfile.get();
        else
            return null;
    }

    private ActivationCode generateActivationCode(UserProfile userProfile) {

        ActivationCode activationCode = new ActivationCode(ActivationCodeGenerator.getActivationCode(),true,false,userProfile);

        activationCodeRepository.save(activationCode);

        return activationCode;
    }

    private ActivationCodeTimer startActivationCodeTimer(ActivationCode activationCode) {

        ActivationCodeTimer activationCodeTimer = new ActivationCodeTimer(activationCodeRepository, activationCode.getCode());
        activationCodeTimer.startTimer();

        return activationCodeTimer;
    }

    public void registerUserProfile(UserProfileDTO userProfileDTO) {

        try{
            userProfileValidator.userExists(userProfileDTO);
            UserProfile userProfile = userProfileMapper.mapToUserProfile(userProfileDTO, false);
            Address address = AddressMapper.mapToAddress(userProfileDTO,userProfile);

            ActivationCode activationCode = generateActivationCode(userProfile);
            ActivationCodeTimer activationCodeTimer = startActivationCodeTimer(activationCode);

            userProfileRepository.save(userProfile);
            addressRepository.save(address);

            activationCodeDTOList.add(new ActivationCodeDTO(activationCode.getCode(), userProfileDTO, activationCodeTimer));
            setResponse("User created, activation code: " + activationCode.getCode(), HttpStatus.CREATED);
        }catch(RegistrationException ex) {
            System.out.println(ex.getMessage());
            setResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public void login(LoggingUserDTO loggingUserDTO) {

        Optional<UserProfile> user = userProfileRepository.findByEmail(loggingUserDTO.getEmail());

        System.out.println("LOGGED USER: " + user.get().toString());

        user.ifPresent(u -> {

            if(authService.checkPassword(loggingUserDTO.getPassword(), u.getPassword()) && u.isActive()){

                Map<String,String> tokens = authService.generateTokens(loggingUserDTO.getEmail(),
                        loggingUserDTO.getPassword());

                setResponse(String.format("User logged in. Access Token: %s\nRefresh Token: %s",
                        tokens.get(authService.ACCESS_TOKEN), tokens.get(authService.REFRESH_TOKEN)),
                        HttpStatus.OK);
            }
        });
    }

    public void confirmUserProfile(String code, String username){

        try {
            Optional<ActivationCode> activationCode = Optional.ofNullable(activationCodeRepository.findByCode(code));
            if (activationCode.isEmpty()) throw new ConfirmationException(propertyReader.getProperty("activation.wrongCode"));

            if(activationCode.get().isActive()) {
                UserProfile userProfile = activationCode.get().getUserProfile();
                if (code.equals(activationCode.get().getCode())
                        && userProfile.getEmail().equals(username)) {

                    activateUserProfile(userProfile, activationCode.get());
                    setResponse(propertyReader.getProperty("confirmation.successful"),
                                            HttpStatus.ACCEPTED);
                } else
                    throw new ConfirmationException("activation.wrongCode");
            };
        }catch(ConfirmationException ex) {
            System.out.println(ex.getMessage());
            setResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    public String refreshActivationCode(UserProfile userProfile) {

        ActivationCode activationCode = generateActivationCode(userProfile);
        ActivationCodeTimer activationCodeTimer = startActivationCodeTimer(activationCode);

        return activationCode.getCode();
    }

    private Address modifyAddress(UserProfileDTO changedUserDTO,
                                  Address actualAddress) {

        actualAddress.setCity(changedUserDTO.getCity());
        actualAddress.setCountry(changedUserDTO.getCountry());
        actualAddress.setStreet(changedUserDTO.getStreet());
        actualAddress.setZipCode(changedUserDTO.getZipCode());

        return actualAddress;
    }

    public void modifyUserProfile(UserProfileDTO changedUserDTO,
                                  UserProfileDTO actualUserDTO) {

        UserProfile modifiedUser = modifyUser(changedUserDTO, actualUserDTO);
        if(modifiedUser != null) {
            userProfileRepository.save(modifiedUser);
            Address address = addressRepository.findByUser(modifiedUser);

            addressRepository.save(modifyAddress(changedUserDTO, address));
            setResponse(String.format("User with id %d modified", modifiedUser.getId()),HttpStatus.CREATED);
        } else
            setResponse("User not found", HttpStatus.BAD_REQUEST);
    }

    private void activateUserProfile(UserProfile userProfile, ActivationCode activationCode) {

        userProfile.setActive(true);
        activationCode.setConfirmed(true);
        activationCode.setActive(false);

        userProfileRepository.save(userProfile);
        activationCodeRepository.save(activationCode);
        ActivationCodeDTO activationCodeDTO = activationCodeDTOList.stream()
                                                                   .filter(o -> o.getCode().equals(activationCode.getCode()))
                                                                   .findFirst().get();
        activationCodeDTO.getActivationCodeTimer().stopTimer();
    }
}
