package com.pusproject.services;

import com.pusproject.dtos.ProjectDTO;
import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.TeamMemberDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.exceptions.UserNotFound;
import com.pusproject.mappers.ProjectMapper;
import com.pusproject.mappers.UserProfileMapper;
import com.pusproject.models.Project;
import com.pusproject.models.Team;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.TeamRepository;
import com.pusproject.repositories.UserProfileRepository;
import com.pusproject.validators.ProjectValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Getter
public class TeamService extends OverallService{

    private final TeamRepository teamRepository;
    private final ProjectRepository projectRepository;
    private final UserProfileRepository userProfileRepository;
    private final ProjectMapper projectMapper;
    private final UserProfileMapper userProfileMapper;


    private boolean isUserTeamMember(int userId, int projectId) {

        Optional<List<UserProfile>> projects = userProfileRepository.findByProjectId(projectId);

         return  projects.map(p -> !p.stream()
                         .filter(up -> up.getId() == userId)
                         .collect(Collectors.toList())
                         .isEmpty())
                .orElse(false);
    }

    private void addNewTeamMember(int userId, Project project) {

        UserProfile user = userProfileRepository.findById(userId).get();
        boolean isUserInProject = userProfileRepository.isUserInProject(userId, project.getId());

        if (!isUserInProject) {

            List<Project> userProjects = user.getProjects();
            userProjects.add(project);
            user.setProjects(userProjects);
            userProfileRepository.save(user);
        }

        setResponse("New member added", HttpStatus.CREATED);
    }

    private void addNewTeamMember(Project project, UserProfileDTO actualUser, TeamMemberDTO teamMember) {

        ProjectDTO projectDTO = projectMapper.mapToProjectDTO(project);

        if (ProjectValidator.isUserProjectOwner(actualUser.getId(), projectDTO)
                && !isUserTeamMember(teamMember.getId(), teamMember.getProjectId())) {

            addNewTeamMember(teamMember.getId(), project);

        } else if (isUserTeamMember(teamMember.getId(), teamMember.getProjectId()))
            setResponse("Member already existed", HttpStatus.ALREADY_REPORTED);
        else
            setResponse("Error", HttpStatus.NOT_ACCEPTABLE);
    }

    public void addNewTeamMember(TeamMemberDTO teamMemberDTO,
                                 UserProfileDTO userProfileDTO) {

        Optional<Project> project = projectRepository.findById(teamMemberDTO.getProjectId());

        try {

            Optional<UserProfile> userProfile = userProfileRepository.findById(teamMemberDTO.getId());

            if(userProfile.isPresent()) {

                project.ifPresentOrElse(p -> {

                    addNewTeamMember(p, userProfileDTO, teamMemberDTO);
                }, () -> setResponse(String.format("Project with id %d not found", teamMemberDTO.getProjectId())
                        , HttpStatus.NOT_FOUND));
            }else
                throw new UserNotFound("User with given id not found");
        }catch(UserNotFound ex) {
            setResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
        }

    }


    public List<UserProfileDTO> getTeamMembers(int projectId,
                                 UserProfileDTO loggedUser) {

        if(ProjectValidator.isUserMemberOfTheProject(projectId,loggedUser)) {

            Optional<List<UserProfile>> teamMembers = userProfileRepository.findByProjectId(projectId);
            List<UserProfileDTO> teamMembersDTO = new ArrayList<>();

            teamMembers.ifPresent(members -> {

                teamMembersDTO.addAll(members.stream()
                        .map(user -> userProfileMapper.mapToUserProfileDTO(user))
                        .collect(Collectors.toList()));
            });

            return teamMembersDTO;
        }
        return Collections.emptyList();
    }

    public void deleteTeamMember(TeamMemberDTO teamMemberDTO,
                                 UserProfileDTO userProfileDTO) {

        int projectId = teamMemberDTO.getProjectId();
        Optional<Project> project = projectRepository.findById(projectId);
        Optional<UserProfile> user = userProfileRepository.findById(userProfileDTO.getId());

        user.ifPresentOrElse(u -> {

            project.ifPresentOrElse(p -> {

                ProjectDTO projectDTO = projectMapper.mapToProjectDTO(p);
                if(ProjectValidator.isUserProjectOwner(userProfileDTO.getId(), projectDTO)
                    && user.isPresent()) {

                    userProfileRepository.deleteById(userProfileDTO.getId());
                    setResponse("Member deleted", HttpStatus.ACCEPTED);
                } else
                    setResponse("Member is not the member of the team", HttpStatus.NOT_FOUND);
            }, () -> setResponse(String.format("Project with id %d not found", teamMemberDTO.getProjectId())
                , HttpStatus.NOT_FOUND));
        }, () -> setResponse(String.format("User with id %d not found", teamMemberDTO.getId())
                , HttpStatus.NOT_FOUND));
    }

}
