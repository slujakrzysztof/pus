package com.pusproject.services;

import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.TaskDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.mappers.TaskMapper;
import com.pusproject.models.Project;
import com.pusproject.models.Task;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.TaskRepository;
import com.pusproject.validators.ProjectValidator;
import io.netty.util.internal.StringUtil;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Setter
@Getter
public class TaskService extends OverallService{
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final TaskMapper taskMapper;

    private boolean isUserAuthorOfTheTask(UserProfileDTO userProfileDTO,
                                          int taskId) {

        return userProfileDTO.getId() == taskId;
    }

    private Task modifyTask(Task actualTask, TaskDTO modifiedTask) {

        actualTask.setName(modifiedTask.getName());
        actualTask.setDescription(modifiedTask.getDescription());
        actualTask.setPriority(modifiedTask.getPriority());
        actualTask.setEndDate(modifiedTask.getEndDate());

        return actualTask;
    }

    private Task modifyTask(TaskDTO taskDTO,
                            int taskId) {

        //Task modifiedTask = taskMapper.mapToTask(taskDTO);
        Project project = projectRepository
                .findById(taskDTO.getProjectId()).get();
        List<Task> tasks = project.getTasks();

        Task actualTask = taskRepository.findById(tasks.stream()
                .filter(task -> task.getId() == taskId)
                .findFirst().get().getId()).get();

        int index = tasks.indexOf(actualTask);

        tasks.set(index, modifyTask(actualTask, taskDTO));

        project.setTasks(tasks);

        projectRepository.save(project);
        return  actualTask;

    }

    private void addTask(Project project, int projectId, UserProfileDTO actualProfile, TaskDTO taskDTO) {

        if (ProjectValidator.isUserMemberOfTheProject(projectId, actualProfile)) {
            Task task = taskMapper.mapToTask(taskDTO);
            project.getTasks().add(task);

            taskRepository.save(task);
            projectRepository.save(project);

            setResponse("Task added", HttpStatus.OK);
        } else
            setResponse("User is not the member of the project", HttpStatus.BAD_GATEWAY);
    }

    public void addTask(TaskDTO taskDTO,
                        int projectId,
                        UserProfileDTO userProfileDTO) {

        Optional<Project> project = projectRepository.findById(projectId);
        Optional<Task> task = taskRepository.findExistingTask(taskDTO.getName(),
                projectId,
                taskDTO.getAuthorId());

        if(!task.isPresent()) {

            project.ifPresentOrElse(p -> {

                addTask(p, projectId, userProfileDTO, taskDTO);
            }, () -> setResponse("Task cannot be added because project doesn't exist",
                    HttpStatus.NOT_FOUND));
        }else
            setResponse("Task exists or not found", HttpStatus.NOT_FOUND);
    }


    public List<TaskDTO> showTasksForProject(int projectId,
                                             UserProfileDTO userProfileDTO) {

        if (ProjectValidator.isUserMemberOfTheProject(projectId, userProfileDTO)) {

            List<TaskDTO> tasks = taskRepository.findByProjectId(projectId)
                    .get().stream()
                    .map(task -> taskMapper.mapToTaskDTO(task, projectId))
                    .collect(Collectors.toList());

            responseDTO.setStatus(HttpStatus.OK);

            return tasks;
        }
        return Collections.emptyList();
    }

    public String getTaskDetails(int taskId) {

        Optional<Task> task = taskRepository.findById(taskId);

        return task.map(t -> t.getDescription())
                .orElse("");
    }

    public void modifyTask(int taskId,
                           TaskDTO taskDTO,
                           UserProfileDTO userProfileDTO) {

        Optional<Task> task = taskRepository.findById(taskId);

        task.ifPresentOrElse(t -> {

            if(isUserAuthorOfTheTask(userProfileDTO, taskId)) {

                taskRepository.save(modifyTask(taskDTO, taskId));
                setResponse("Task modified", HttpStatus.CREATED);
            }else
                setResponse("User is not the author of the task", HttpStatus.BAD_GATEWAY);
        }, () -> setResponse("Task not found", HttpStatus.NOT_FOUND));
    }

    public void deleteTask(int taskId,
                           UserProfileDTO userProfileDTO) {

        if(isUserAuthorOfTheTask(userProfileDTO, taskId)) {

            Optional<Task> task = taskRepository.findById(taskId);

            task.ifPresentOrElse(t -> {
                taskRepository.delete(t);
                setResponse(String.format("Task with id: %d deleted", taskId),
                        HttpStatus.ACCEPTED);
            }, () -> setResponse(String.format("Task with id: %d not found", taskId),
                    HttpStatus.NOT_FOUND));
        }
    }

}
