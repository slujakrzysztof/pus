package com.pusproject.services;

import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.mappers.UserProfileMapper;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.UserProfileRepository;
import com.pusproject.security.TokenGenerator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService extends OverallService{

    public final String ACCESS_TOKEN = "accessToken";
    public final String REFRESH_TOKEN = "refreshToken";
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final TokenGenerator tokenGenerator;
    private final UserProfileRepository userProfileRepository;
    private final UserProfileMapper userProfileMapper;
    private Authentication authentication;


    public boolean checkPassword(String currentPassword, String userPassword) {

        return passwordEncoder.matches(currentPassword, userPassword);
    }

    public void refreshAccessToken(String refreshToken) {

        setResponse(String.format("New access token: %s",
                tokenGenerator.generateAccessToken(authentication)), HttpStatus.OK);
    }

    public Map<String, String> generateTokens(String username, String password) {

        Map<String, String> tokensMap = new HashMap<>();
        authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        tokensMap.put(ACCESS_TOKEN, tokenGenerator.generateAccessToken(authentication));
        tokensMap.put(REFRESH_TOKEN, tokenGenerator.generateRefreshToken(authentication));

        return tokensMap;
    }

    public String generateAccessToken(String username, String password) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return tokenGenerator.generateAccessToken(authentication);
    }

    private String getUsernameFromToken(String token) {

        return tokenGenerator.getUsernameFromJWT(token);
    }

    public UserProfileDTO getLoggedUser(String token) {

        String username = getUsernameFromToken(token);
        Optional<UserProfile> userProfile = userProfileRepository.findByEmail(username);

        if(userProfile.isPresent())
            return userProfileMapper.mapToUserProfileDTO(userProfile.get());

        return null;
    }

}
