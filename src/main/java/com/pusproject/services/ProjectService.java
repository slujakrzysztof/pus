package com.pusproject.services;

import com.pusproject.dtos.ProjectDTO;
import com.pusproject.dtos.ResponseDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.mappers.ProjectMapper;
import com.pusproject.models.Project;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.UserProfileRepository;
import com.pusproject.validators.ProjectValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Setter
@Getter
public class ProjectService extends OverallService{
    private final ProjectRepository projectRepository;
    private final UserProfileRepository userProfileRepository;
    private final ProjectMapper projectMapper;


    public void addProject(ProjectDTO projectDTO,
                           UserProfileDTO userProfileDTO) {

        Optional<Project> project = projectRepository.findExistingProject(projectDTO.getName(), userProfileDTO.getId());

        System.out.println("PROJECTTT: " + project);

        project.ifPresentOrElse(p ->
                setResponse(String.format("Project with id: %d already existed!", p.getId()), HttpStatus.ALREADY_REPORTED),
         () -> {

            Project newProject = projectMapper.mapToProject(projectDTO, userProfileDTO.getId());
            Project savedProject = projectRepository.save(newProject);
            setResponse("Project created with id: " + savedProject.getId(), HttpStatus.CREATED);
        });
    }

    public void getProjectDetails(int id,
                                  UserProfileDTO userProfileDTO) {

        Optional<Project> project = projectRepository.findById(id);
        String details = "";

        if(project.isPresent()
                && ProjectValidator.isUserMemberOfTheProject(project.get().getId(), userProfileDTO)) {

            details = project.map(p -> p.getDescription()).orElse(null);
            responseDTO.setStatus(HttpStatus.OK);
        } else
            responseDTO.setStatus(HttpStatus.NOT_FOUND);

        responseDTO.setMessage(details);
    }

    public void deleteProject(int id,
                              UserProfileDTO userProfileDTO) {

        Optional<Project> project = projectRepository.findById(id);
        int projectOwnerId = project.get().getOwner().getId();

        if(project.isPresent() && (projectOwnerId == userProfileDTO.getId()))
            projectRepository.delete(project.get());

        setResponse(String.format("Project with id = %s deleted", id), HttpStatus.ACCEPTED);
    }
}
