package com.pusproject.services;

import com.pusproject.models.ActivationCode;
import com.pusproject.repositories.ActivationCodeRepository;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ActivationCodeGenerator {

    private static final int NUMBER_OF_CHAR_IN_CODE = 6;

    @Autowired
    private ActivationCodeRepository activationCodeRepository;

    private static ActivationCodeRepository repository;

    @PostConstruct
    private void init() {
        repository = activationCodeRepository;
    }

    public static String getActivationCode() {

        String code;

        while(true) {
             code = generateActivationCode();
            Optional<ActivationCode> existingCode = Optional.ofNullable(repository.findByCode(code));
            if(existingCode.isPresent()) continue;
            else break;
        }

        return code;
    }

    private static String generateActivationCode() {

        return RandomStringUtils.randomAlphabetic(NUMBER_OF_CHAR_IN_CODE);
    }
}
