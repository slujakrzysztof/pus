package com.pusproject.properties;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
public class PropertyReader {

    private Properties property = new Properties();

    private final String FILENAME = "messages.properties";


@PostConstruct
    public void setConnection(){

    System.out.println("HERE");

    ClassLoader loader = Thread.currentThread().getContextClassLoader();

        InputStream input = null;
        try {
            input = loader.getResourceAsStream(FILENAME);
            property.load(input);
        } catch(IOException ex) {
            ex.printStackTrace();
        } catch(NullPointerException ex2){
            ex2.printStackTrace();
        }finally {
            if(input != null) {
                try {
                    input.close();
                } catch(IOException ex1) {
                    ex1.printStackTrace();
                }
            }
        }
    }

    //Getting property by specific key
    public String getProperty(String key){
        return property.getProperty(key);
    }
}
