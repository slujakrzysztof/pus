package com.pusproject.dtos;

import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ProjectDTO {
    @NonNull
    private String name;
    @NonNull
    private String description;
    @NonNull
    private LocalDate startDate;
    @NonNull
    private LocalDate endDate;
    @Nullable
    private List<Integer> members;
    @Nullable
    private int ownerId;
    @Nullable
    private List<Integer> tasks;
}
