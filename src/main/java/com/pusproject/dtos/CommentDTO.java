package com.pusproject.dtos;

import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommentDTO {
    private String content;
    private int taskId;
    private int projectId;
    private int id;
    @Nullable
    private String author;

}
