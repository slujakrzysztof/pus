package com.pusproject.dtos;

import lombok.Data;

import java.util.List;

@Data
public class TeamDTO {

    private String name;
    private List<UserProfileDTO> members;
    private UserProfileDTO owner;

}
