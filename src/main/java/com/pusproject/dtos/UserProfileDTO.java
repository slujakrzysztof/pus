package com.pusproject.dtos;

import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class UserProfileDTO {

    @NonNull
    private String email;
    @NonNull
    private String password;
    @NonNull
    private String name;
    @NonNull
    private String surname;
    @NonNull
    private String country;
    @NonNull
    private String city;
    @NonNull
    private String street;
    private int zipCode;
    private int id;
    @Nullable
    private List<Integer> projectsId;

}
