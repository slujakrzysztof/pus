package com.pusproject.dtos;

import lombok.Data;

@Data
public class LoggingUserDTO {

    private String email;
    private String password;
}
