package com.pusproject.dtos;

import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class TaskDTO {
    private String name;
    private String description;
    private String priority;
    private LocalDate endDate;
    private int authorId;
    @Nullable
    private int id;
    @Nullable
    private int projectId;
}
