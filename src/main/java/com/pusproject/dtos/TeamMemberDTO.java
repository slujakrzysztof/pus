package com.pusproject.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamMemberDTO {

    private int id;
    private int projectId;
}
