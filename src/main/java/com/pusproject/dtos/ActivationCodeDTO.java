package com.pusproject.dtos;

import com.pusproject.timer.ActivationCodeTimer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ActivationCodeDTO {

    private String code;
    private UserProfileDTO userProfileDTO;
    private ActivationCodeTimer activationCodeTimer;
}
