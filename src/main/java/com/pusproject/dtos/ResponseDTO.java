package com.pusproject.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@Data
public class ResponseDTO {

    private String message;
    private HttpStatus status;

    public void setResponse(String message, HttpStatus status) {

        this.setMessage(message);
        this.setStatus(status);
    }

}
