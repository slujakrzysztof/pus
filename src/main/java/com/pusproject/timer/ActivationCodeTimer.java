package com.pusproject.timer;

import com.pusproject.models.ActivationCode;
import com.pusproject.repositories.ActivationCodeRepository;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class ActivationCodeTimer extends Timer {

    private final int CODE_ACTIVE_TIME = 600_000;

    private final ActivationCodeRepository activationCodeRepository;
    private String code;
    private ActivationCodeTimer thisTimer;

    public ActivationCodeTimer(ActivationCodeRepository activationCodeRepository, String code){
        super();
        this.activationCodeRepository = activationCodeRepository;
        this.code = code;
        thisTimer = this;

    }

    public void startTimer() {

        this.schedule(new TimerTask() {
            @Override
            public void run() {
                ActivationCode activationCode = getActivationCode(code);
                if(activationCode != null) {

                    activationCode.setActive(false);
                    activationCodeRepository.save(activationCode);
                }else{
                    System.out.println("null");
                }
                stopTimer();
            }
        }, CODE_ACTIVE_TIME);
    }

    public void stopTimer() {

        this.cancel();
    }

    public ActivationCode getActivationCode(String code) {

        Optional<ActivationCode> activationCode = Optional.ofNullable(activationCodeRepository.findByCode(code));
        if(activationCode.isPresent()) return activationCode.get();
        return null;
    }

    private boolean isCodeConfirmed(String code) {

        Optional<ActivationCode> activationCode = Optional.ofNullable(getActivationCode(code));
        if(activationCode.isPresent()) return activationCode.get().isConfirmed();
        return false;
    }
}
