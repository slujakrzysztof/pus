package com.pusproject.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TokenGenerator {

    private final long ACCESS_TOKEN_EXPIRATION_TIME = 5 * 60 * 1000;
    private final long REFRESH_TOKEN_EXPIRATION_TIME = 60 * 60 * 1000;
    private final String TOKEN_SECRET = "secretToken";


    public String generateToken(String username, Date expirationDate) {

        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, TOKEN_SECRET)
                .compact();
        return token;
    }

    public String generateAccessToken(Authentication authentication) {

        String username = authentication.getName();
        Date currentDate = new Date();
        Date expirationDate = new Date(currentDate.getTime() + ACCESS_TOKEN_EXPIRATION_TIME);

        return generateToken(username, expirationDate);
    }

    public String generateRefreshToken(Authentication authentication) {

        String username = authentication.getName();
        Date currentDate = new Date();
        Date expirationDate = new Date(currentDate.getTime() + REFRESH_TOKEN_EXPIRATION_TIME);

        return generateToken(username, expirationDate);
    }

    public String getUsernameFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(TOKEN_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(TOKEN_SECRET).parseClaimsJws(token);
            return true;
        } catch (Exception ex) {
            throw new AuthenticationCredentialsNotFoundException("JWT token was expired or incorrect. Generate new token under /token/refresh or login again.");
        }
    }
}
