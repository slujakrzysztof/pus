package com.pusproject.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@AllArgsConstructor
@NoArgsConstructor
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    public final String ACCESS_TOKEN_HEADER_NAME = "accessToken";
    public final String REFRESH_TOKEN_HEADER_NAME = "refreshToken";

    @Autowired
    private TokenGenerator tokenGenerator;
    @Autowired
    private SecurityUserDetailsRepository securityUserDetailsRepository;




    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        String token = getTokenFromRequest(request);
        try {
            if (token != null && tokenGenerator.validateToken(token)) {
                String username = tokenGenerator.getUsernameFromJWT(token);

                UserDetails userDetails = securityUserDetailsRepository.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null,
                        userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }catch(AuthenticationCredentialsNotFoundException ex) {

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write("An error occurred: " + ex.getMessage());
            return;
        }

        filterChain.doFilter(request, response);

    }

    private final String getTokenFromRequest(HttpServletRequest request) {

        String accessToken = request.getHeader("accessToken");
        String refreshToken = request.getHeader("refreshToken");

        boolean isAccessTokenAvailable = accessToken != null && StringUtils.isNotBlank(accessToken) && StringUtils.isNotEmpty(accessToken);

        if(isAccessTokenAvailable) {
            return accessToken.replaceAll("Bearer ","").stripLeading();
        }

        return null;
    }
}
