package com.pusproject.security;

import com.pusproject.models.UserProfile;
import com.pusproject.repositories.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;


@RequiredArgsConstructor
@Component
public class SecurityUserDetailsRepository implements UserDetailsService {

    private final UserProfileRepository userProfileRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserProfile userProfile = userProfileRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User with given email doesn't exist!"));

        return new User(userProfile.getEmail(), userProfile.getPassword(), Collections.emptyList());

    }
}
