package com.pusproject.mappers;

import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.models.Address;
import com.pusproject.models.UserProfile;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public static Address mapToAddress(UserProfileDTO userProfileDTO, UserProfile userProfile) {

        Address address = new Address();

        address.setCity(userProfileDTO.getCity());
        address.setCountry(userProfileDTO.getCountry());
        address.setStreet(userProfileDTO.getStreet());
        address.setZipCode(userProfileDTO.getZipCode());
        address.setUser(userProfile);

        return address;

    }

}
