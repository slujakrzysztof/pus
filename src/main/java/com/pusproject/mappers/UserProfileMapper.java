package com.pusproject.mappers;

import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.models.Address;
import com.pusproject.models.Project;
import com.pusproject.models.Team;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.AddressRepository;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserProfileMapper {

    private final ProjectRepository projectRepository;
    private final AddressRepository addressRepository;
    private final TeamRepository teamRepository;
    private final PasswordEncoder passwordEncoder;

    public UserProfile mapToUserProfile(UserProfileDTO userProfileDTO, boolean isActive) {

        UserProfile userProfile = new UserProfile();


        List<Project> projects = userProfileDTO.getProjectsId() != null ?
                userProfileDTO.getProjectsId()
                .stream()
                .map(id -> projectRepository.findById(id).get())
                .collect(Collectors.toList()) : Collections.emptyList();

        userProfile.setName(userProfileDTO.getName());
        userProfile.setSurname(userProfileDTO.getSurname());
        userProfile.setEmail(userProfileDTO.getEmail());
        userProfile.setPassword(passwordEncoder.encode(userProfileDTO.getPassword()));
        userProfile.setActive(isActive);
        userProfile.setProjects(projects);

        return userProfile;
    }

    public UserProfileDTO mapToUserProfileDTO(UserProfile userProfile) {

        List<Project> projects = userProfile.getProjects();
        List<Integer> projectsId = projects.stream().map(project -> project.getId()).collect(Collectors.toList());
        Address address = addressRepository.findByUser(userProfile);

        return UserProfileDTO.builder()
                .name(userProfile.getName())
                .surname(userProfile.getSurname())
                .id(userProfile.getId())
                .password(userProfile.getPassword())
                .email(userProfile.getEmail())
                .city(address.getCity())
                .street(address.getStreet())
                .country(address.getCountry())
                .zipCode(address.getZipCode())
                .projectsId(projectsId)
                .build();
    }

}
