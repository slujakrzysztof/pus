package com.pusproject.mappers;

import com.pusproject.dtos.TaskDTO;
import com.pusproject.models.Task;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TaskMapper {

    private final UserProfileRepository userProfileRepository;

    public Task mapToTask(TaskDTO taskDTO) {

        Task task = new Task();
        UserProfile author = userProfileRepository.findById(taskDTO.getAuthorId()).get();

        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setPriority(taskDTO.getPriority());
        task.setEndDate(taskDTO.getEndDate());
        task.setAuthor(author);

        return task;
    }

    public TaskDTO mapToTaskDTO(Task task, int projectId) {

        return TaskDTO.builder()
                .name(task.getName())
                .id(task.getId())
                .description(task.getDescription())
                .endDate(task.getEndDate())
                .authorId(task.getAuthor().getId())
                .projectId(projectId)
                .build();
    }

}
