package com.pusproject.mappers;

import com.pusproject.dtos.ProjectDTO;
import com.pusproject.models.Project;
import com.pusproject.models.Task;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.TaskRepository;
import com.pusproject.repositories.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProjectMapper {

    private final UserProfileRepository userProfileRepository;
    private final TaskRepository taskRepository;

    public Project mapToProject(ProjectDTO projectDTO, int ownerId) {

        Project project = new Project();

        List<Task> tasks = projectDTO.getTasks() != null ?
                projectDTO.getTasks().stream()
                        .map(id -> taskRepository.findById(id).get())
                        .collect(Collectors.toList()) : Collections.emptyList();

        UserProfile owner = userProfileRepository.findById(ownerId).get();

        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setEndDate(projectDTO.getEndDate());
        project.setTasks(tasks);
        project.setOwner(owner);

        return project;
    }

    public ProjectDTO mapToProjectDTO(Project project) {

        UserProfile owner = userProfileRepository.findById(project.getOwner().getId()).get();
        List<Integer> tasks = project.getTasks()
                .stream()
                .map(task -> task.getId()).collect(Collectors.toList());
        List<UserProfile> members = userProfileRepository.findByProjectId(project.getId()).get();
        List<Integer> membersId = members.stream()
                .map(member -> member.getId())
                .collect(Collectors.toList());

        return ProjectDTO.builder()
                .name(project.getName())
                .description(project.getDescription())
                .startDate(project.getStartDate())
                .endDate(project.getEndDate())
                .ownerId(owner.getId())
                .tasks(tasks)
                .members(membersId)
                .build();
    }

}
