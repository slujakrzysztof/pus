package com.pusproject.mappers;

import com.pusproject.dtos.CommentDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.models.Comment;
import com.pusproject.models.Project;
import com.pusproject.models.Task;
import com.pusproject.models.UserProfile;
import com.pusproject.repositories.ProjectRepository;
import com.pusproject.repositories.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class CommentMapper {

    private final ProjectRepository projectRepository;
    private final UserProfileRepository userProfileRepository;
    public Comment mapToComment(CommentDTO commentDTO, Task task, UserProfileDTO userProfileDTO) {

        Comment comment = new Comment();
        UserProfile user = userProfileRepository.findById(userProfileDTO.getId()).get();

        comment.setContent(commentDTO.getContent());
        comment.setTask(task);
        comment.setAuthor(user);

        return comment;
    }

    public CommentDTO mapToCommentDTO(Comment comment){

        Project project = projectRepository.findByTaskId(comment.getTask().getId()).get();

        return CommentDTO.builder()
                         .content(comment.getContent())
                         .taskId(comment.getTask().getId())
                         .projectId(project.getId())
                         .author(comment.getAuthor().getEmail())
                         .id(comment.getId())
                         .build();
    }

}
