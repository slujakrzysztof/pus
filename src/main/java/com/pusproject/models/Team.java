package com.pusproject.models;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.List;

@Entity
@Table
@Getter
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name = "memberId")
    private List<UserProfile> teamMember;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private UserProfile teamOwner;

}
