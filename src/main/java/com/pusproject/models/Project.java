package com.pusproject.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
    @OneToMany(cascade = CascadeType.ALL,
               orphanRemoval = true)
    @JoinColumn(name = "taskId")
    private List<Task> tasks;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ownerId", referencedColumnName = "id")
    private UserProfile owner;


}
