package com.pusproject.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivationCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String code;

    @Column
    private boolean active;

    @Column
    private boolean confirmed;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserProfile userProfile;

    public ActivationCode(String code, boolean active, boolean confirmed, UserProfile userProfile) {
        this.code = code;
        this.active = active;
        this.confirmed = confirmed;
        this.userProfile = userProfile;
    }
}
