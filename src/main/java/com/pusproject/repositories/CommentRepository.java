package com.pusproject.repositories;

import com.pusproject.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    //TODO fingbyid
    public Comment findByContent(String content);

    @Query("Select c from Comment c JOIN c.task t WHERE t.id = :taskId")
    Optional<List<Comment>> findByTaskId(int taskId);

    @Query("select c from Comment c join c.task t where c.id = :commentId and t.id = :taskId")
    Optional<Comment> findSpecificCommentForSpecificTask(@Param("taskId") int taskId,
                                                         @Param("commentId") int commentId);

}
