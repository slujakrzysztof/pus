package com.pusproject.repositories;

import com.pusproject.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    @Query("SELECT t FROM Project p JOIN p.tasks t WHERE p.id = :projectId")
    Optional<List<Task>> findByProjectId(int projectId);

    @Query("select t from Task t join Project p where p.id = :projectId and t.name like :name and t.author.id = :authorId")
    Optional<Task> findExistingTask(@Param("name") String name,
                                    @Param("projectId") int projectId,
                                    @Param("authorId") int authorId);

}
