package com.pusproject.repositories;

import com.pusproject.models.Project;
import com.pusproject.models.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Integer deleteById(int id);
    @Query("select p from Project p join p.tasks t where t.id = :taskId")
    Optional<Project> findByTaskId(int taskId);
    @Query(value = "select p from Project p where p.name = :name and p.owner.id = :ownerId")
    Optional<Project> findExistingProject(@Param("name") String name,
                                          @Param("ownerId") int id);



}
