package com.pusproject.repositories;

import com.pusproject.models.Project;
import com.pusproject.models.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Integer> {

    Optional<UserProfile> findByEmail(String email);
    @Query("SELECT u FROM UserProfile u JOIN u.projects p WHERE p.id = :projectId")
    Optional<List<UserProfile>> findByProjectId(@Param("projectId") int projectId);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM UserProfile u JOIN u.projects p WHERE u.id = :userId and p.id = :projectId")
    boolean isUserInProject(@Param("userId") int userId, @Param("projectId") int projectId);

    @Query("select u from UserProfile u join u.projects p where p.id = :projectId")
    List<UserProfile> listProjectMember(@Param("projectId") int projectId);
}
