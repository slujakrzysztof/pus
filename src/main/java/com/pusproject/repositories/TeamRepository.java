package com.pusproject.repositories;

import com.pusproject.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {


    Optional<Team> findByName(String name);
    @Query(value = "select t from Team t join UserProfile u where t.teamOwner.id = :ownerId", nativeQuery = true)
    Optional<Team> findByOwnerId(int ownerId);

    @Query("select t from Team t join UserProfile u where t.teamMember = :userId")
    Optional<Team> findByUserId(int userId);

}