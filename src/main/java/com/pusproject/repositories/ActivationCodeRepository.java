package com.pusproject.repositories;

import com.pusproject.models.ActivationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivationCodeRepository extends JpaRepository<ActivationCode, Integer> {

    public ActivationCode findByCode(String code);
}
