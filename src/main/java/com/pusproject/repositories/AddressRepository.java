package com.pusproject.repositories;

import com.pusproject.models.Address;
import com.pusproject.models.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

    public Address findByUser(UserProfile user);
}
