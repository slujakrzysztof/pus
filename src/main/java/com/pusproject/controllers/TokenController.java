package com.pusproject.controllers;

import com.pusproject.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")

public class TokenController extends AuthenticationController{

    public TokenController(AuthService authService) {
        super(authService);
    }

    @GetMapping("/refresh")
    public ResponseEntity<String> refreshToken(@RequestHeader("refreshToken") String refreshToken) {

        authService.refreshAccessToken(refreshToken);
        return getResponse(authService);
    }
}
