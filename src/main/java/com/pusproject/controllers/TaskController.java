package com.pusproject.controllers;

import com.pusproject.dtos.TaskDTO;
import com.pusproject.services.AuthService;
import com.pusproject.services.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController extends AuthenticationController{

    private final TaskService taskService;

    public TaskController(AuthService authService, TaskService taskService) {
        super(authService);
        this.taskService = taskService;
    }


    @PostMapping("/add/{id}")
    public ResponseEntity<String> addNewTask(@RequestBody TaskDTO taskDTO,
                                             @PathVariable("id") int projectId,
                                             @RequestHeader("accessToken") String accessToken) {

        taskService.addTask(taskDTO, projectId, getLoggedUser(accessToken));
        return getResponse(taskService);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<List<TaskDTO>> showTasks(@PathVariable("id") int projectId,
                                                   @RequestHeader("accessToken") String accessToken) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(taskService.showTasksForProject(projectId, getLoggedUser(accessToken)));
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<String> getTaskDetails(@PathVariable("id") int taskId,
                                                 @RequestHeader("accessToken") String accessToken) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(taskService.getTaskDetails(taskId));
    }

    @PostMapping("/modify/{id}")
    public ResponseEntity<String> modifyTask(@PathVariable("id") int taskId,
                                             @RequestBody TaskDTO taskDTO,
                                             @RequestHeader("accessToken") String accessToken) {

        taskService.modifyTask(taskId, taskDTO, getLoggedUser(accessToken));
        return getResponse(taskService);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteTask(@PathVariable("id") int taskId,
                                             @RequestHeader("accessToken") String accessToken) {

        taskService.deleteTask(taskId, getLoggedUser(accessToken));
        return getResponse(taskService);
    }



}
