package com.pusproject.controllers;


import com.pusproject.dtos.CommentDTO;
import com.pusproject.services.AuthService;
import com.pusproject.services.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController extends AuthenticationController{

    private final CommentService commentService;

    public CommentController(AuthService authService, CommentService commentService) {
        super(authService);
        this.commentService = commentService;
    }

    @PostMapping("/add")
    public ResponseEntity<String> addNewComment(@RequestBody CommentDTO commentDTO,
                                                @RequestHeader("accessToken") String accessToken) {

        commentService.addComment(commentDTO, getLoggedUser(accessToken));
        return getResponse(commentService);
    }

    @GetMapping("/show/{taskId}")
    public ResponseEntity<List<CommentDTO>> getTaskComments(@PathVariable int taskId) {

        return ResponseEntity.status(getStatus(commentService))
                .body(commentService.getTaskComments(taskId));
    }

    @DeleteMapping("/delete/{projectId}/{commentId}")
    public ResponseEntity<String> deleteComment(@PathVariable int commentId,
                                                @PathVariable int projectId,
                                                @RequestHeader("accessToken") String accessToken) {

        commentService.deleteComment(commentId, projectId, getLoggedUser(accessToken));
        return getResponse(commentService);
    }

}
