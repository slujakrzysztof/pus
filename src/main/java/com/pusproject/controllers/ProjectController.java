package com.pusproject.controllers;

import com.pusproject.dtos.ProjectDTO;
import com.pusproject.services.AuthService;
import com.pusproject.services.ProjectService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/project")
public class ProjectController extends AuthenticationController{

    private final ProjectService projectService;

    public ProjectController(AuthService authService, ProjectService projectService) {
        super(authService);
        this.projectService = projectService;
    }

    @PostMapping("/create")
    public ResponseEntity<String> createNewProject(@RequestBody ProjectDTO projectDTO,
                                                   @RequestHeader("accessToken") String accessToken) {

        projectService.addProject(projectDTO, getLoggedUser(accessToken));
        return getResponse(projectService);
    }

    @GetMapping("/details")
    public ResponseEntity<String> getProjectDetails(@RequestParam int id,
                                                    @RequestHeader("accessToken") String accessToken){

        projectService.getProjectDetails(id, getLoggedUser(accessToken));
        return getResponse(projectService);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteProject(@PathVariable int id,
                                                @RequestHeader("accessToken") String accessToken) {

        projectService.deleteProject(id, getLoggedUser(accessToken));
        return getResponse(projectService);
    }

}
