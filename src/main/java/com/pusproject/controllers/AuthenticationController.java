package com.pusproject.controllers;

import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.models.UserProfile;
import com.pusproject.services.AuthService;
import com.pusproject.services.OverallService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RequiredArgsConstructor
@Getter
@Setter
public abstract class AuthenticationController {

    protected final AuthService authService;

    protected UserProfileDTO getLoggedUser(String token) {

        return authService.getLoggedUser(token);
    }

    protected ResponseEntity<String> getResponse(OverallService service) {

        return ResponseEntity
                .status(service.getStatus())
                .body(service.getMessage());
    }

    protected HttpStatus getStatus(OverallService service) {

        return service.getStatus();
    }

}
