package com.pusproject.controllers;

import com.pusproject.dtos.LoggingUserDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.services.AuthService;
import com.pusproject.services.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserProfileController extends AuthenticationController{

    private final UserProfileService userProfileService;

    public UserProfileController(AuthService authService,
                                 UserProfileService userProfileService) {
        super(authService);
        this.userProfileService = userProfileService;
    }

    @PostMapping("/register")
    public ResponseEntity<String> registerNewUser(@RequestBody UserProfileDTO userProfileDTO) {

        userProfileService.registerUserProfile(userProfileDTO);
        return getResponse(userProfileService);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoggingUserDTO loggingUserDTO) {

        userProfileService.login(loggingUserDTO);
        return getResponse(userProfileService);
    }


    @PostMapping("/modify")
    public ResponseEntity<String> modifyUserProfile(@RequestBody UserProfileDTO changedUser,
                                                    @RequestHeader("accessToken") String accessToken) {

        userProfileService.modifyUserProfile(changedUser, getLoggedUser(accessToken));
        return getResponse(userProfileService);
    }

    @PostMapping("/activate/{code}")
    public ResponseEntity<String> activateUser(@RequestBody LoggingUserDTO loggingUserDTO,
                                               @PathVariable String code) {

        userProfileService.confirmUserProfile(code, loggingUserDTO.getEmail());
        return getResponse(userProfileService);
    }

    @PostMapping("/activate/refresh")
    public ResponseEntity<Object> refreshActivationCode(@RequestBody LoggingUserDTO loggingUserDTO) {

        String code =  userProfileService.refreshActivationCode(userProfileService.getUserProfile(loggingUserDTO));
        return ResponseEntity.ok(code);
    }

}
