package com.pusproject.controllers;

import com.pusproject.dtos.TeamMemberDTO;
import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.services.AuthService;
import com.pusproject.services.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/team")
public class TeamController extends AuthenticationController{

    private final TeamService teamService;

    public TeamController(AuthService authService, TeamService teamService) {
        super(authService);
        this.teamService = teamService;
    }


    @PostMapping("/add")
    public ResponseEntity<String> addNewTeamMember(@RequestBody TeamMemberDTO teamMember,
                                                   @RequestHeader("accessToken") String accessToken) {

        teamService.addNewTeamMember(teamMember, getLoggedUser(accessToken));
        return getResponse(teamService);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<List<UserProfileDTO>> getTeamMember(@PathVariable("id") int projectId,
                                                              @RequestHeader("accessToken") String accessToken) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(teamService.getTeamMembers(projectId, getLoggedUser(accessToken)));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteTeamMember(@RequestBody TeamMemberDTO teamMemberDTO,
                                                   @RequestHeader("accessToken") String accessToken) {

        teamService.deleteTeamMember(teamMemberDTO, getLoggedUser(accessToken));
        return getResponse(teamService);
    }


}
