package com.pusproject.exceptions;

public class ConfirmationException extends Exception{

    public ConfirmationException(String message) {
        super(message);
    }
}
