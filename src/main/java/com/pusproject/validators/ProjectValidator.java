package com.pusproject.validators;

import com.pusproject.dtos.ProjectDTO;
import com.pusproject.dtos.UserProfileDTO;

public class ProjectValidator {

    public static boolean isUserMemberOfTheProject(int projectId, UserProfileDTO userProfileDTO) {

        System.out.println("IS: " + userProfileDTO.getProjectsId().contains(projectId));
        return userProfileDTO.getProjectsId().contains(projectId);
    }

    public static boolean isUserProjectOwner(int userId, ProjectDTO project) {

        return project.getOwnerId() == userId;
    }
}
