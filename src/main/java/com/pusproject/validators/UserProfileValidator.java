package com.pusproject.validators;

import com.pusproject.dtos.UserProfileDTO;
import com.pusproject.exceptions.RegistrationException;
import com.pusproject.models.UserProfile;
import com.pusproject.properties.PropertyReader;
import com.pusproject.repositories.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserProfileValidator {

    private final UserProfileRepository userProfileRepository;
    private final PropertyReader propertyReader;

    public boolean userExists(UserProfileDTO userProfileDTO) throws RegistrationException {


        if(this.checkExistenceByEmail(userProfileDTO.getEmail()))
            throw new RegistrationException(propertyReader.getProperty("user.exist"));
        else
            return false;
    }

    private boolean checkExistenceByEmail(String email) {

        Optional<UserProfile> userProfile = userProfileRepository.findByEmail(email);
        System.out.println("U&SR: " + userProfile);
        boolean exists = userProfile.map(user -> true).orElse(false);

        return exists;
    }
}
